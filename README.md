**Tutorial como criar docker compose php - mysql**

1. Criar pasta raiz, e criar pasta php.
2. Dentro da pasta php criar arquivo index.php e adicionar o código para a conexão com o banco e php.
3. Na pasta raiz criar arquivo docker-compose.yml contendo os códigos para criar o mysql e o php, para fazer a conexão entre o mysql e php adicione o seguinte comando:
    
    `links:`

      `- (nome dado ao banco)`

4. Use o comando `docker-compose up -d` e acesse localhost:(porta informada).



**Links de referência**

https://alysivji.github.io/php-mysql-docker-containers.html

https://www.devmedia.com.br/php-e-mysql-conectando-e-exibindo-dados-de-forma-rapida-dica/28526
